---
longname: "Space Quest II: Vohaul's Revenge"
franchise: Space Quest
copyright: "\u00a9 1987 Sierra"
plugin: scummvm_common
wiki: Space_Quest_II
wikipedia: https://en.wikipedia.org/wiki/Space_Quest_II:_Vohaul%27s_Revenge
gameid: sq2

packages:
  spacequest2-data:
    gog:
      url: space_quest_1_2_3
      game: space_quest_2_vohauls_revenge
    steam:
      id: 10110
      path:
      - "common/Space Quest Collection/2016_SpaceQuestCollection/sq2"
      - "common/Space Quest Collection/2016_SpaceQuestCollection/Manuals"
    install:
    - assets
    doc:
    - manual.pdf

files:
  setup_space_quest_1_2_3.exe:
    unpack:
      format: innoextract
      prefix: "Space quest 2"
    provides:
    - assets
    - manual.pdf?gog

  setup_space_quest2_2.1.0.16.exe:
    unpack:
      format: innoextract
    provides:
    - assets
    - manual.pdf?gog

  setup_space_quest_2_-_vohauls_revenge_2.0f_(20240).exe:
    unpack:
      format: innoextract
    provides:
    - assets
    - manual.pdf?gog

  manual.pdf:
    alternatives:
    - manual.pdf?gog
    - Manual.pdf?steam
    install_as: manual.pdf

groups:
  assets: |
    426       28add5125484302d213911df60d2aded logdir
    331       5024d50d42801aaea7170a756218a7e6 object
    444       514534df82237848dfcd760e476a766e picdir
    210       eab6a5f4cb071c906df674e98891a432 snddir
    83        730535011acee0ece94e1a64fe6231e8 sq21589
    720       efb1f93513d57eeca9a539e019df7b89 viewdir
    70313     3e09c8580506212cd3494b5df8ea6cbc vol.0
    161225    fa5415f7cbc6e78c69d94ef3c92d000d vol.1
    411824    f7015465702a24333a0e799ad0d967bb vol.2
    6828      552e2b31bf8825eea5a46e6eebb4cb3e words.tok

  gog archive: |
    5456095   91185d0d657338941a177cb5f6a7c2f3 manual.pdf?gog
    18505218  6ab36bf35f920828cdcb3f3b83156572 setup_space_quest_1_2_3.exe
    22222176  96eff8bcba39afb2b2d0154598ad961d setup_space_quest2_2.1.0.16.exe
    28225200  4f38133414eb0a2364544796c16891cf setup_space_quest_2_-_vohauls_revenge_2.0f_(20240).exe

  steam manual:
    doc: true
    group_members: |
      9005982   a1b4f8093c50d61e7ef17db9d1ca2218 SQManual.pdf?steam
      5357395   f251c4cd6ff3efc87cc756a54768b5c2 Manual.pdf?steam

sha1sums: |
  698adf61f0d28d02d438536795cf07d75b7dfa7e  logdir
  0e77603e5ddd16252e67fb5193aa527e04c8efd9  object
  5c87dfc1ed42f1d081e67a72b77e769ce2465da2  picdir
  79cc5f5d8690319119007bdaf00c84cf86ffa923  snddir
  74e6b6fcef76516ea1be1fc8c584fefe66eb97be  viewdir
  5f9cba6dced32737c5a80f7cf2e805541c9fa422  vol.0
  79963ad0ce237966e8125a3c0b04bb6840212be4  vol.1
  1dd02bf49ccf480574984b3e9022cf54d3b0fcb7  vol.2
  4011fb5b691986f057baeda79c88d225355a59b4  words.tok
  1fe5b92e25729c60672afba30adc2aef8c0ee836  manual.pdf?gog
  d70598bc1145062b5e6d2d5a844a7959e17e11d3  setup_space_quest_1_2_3.exe
  3207cc4264d6842368bbf26ed1094564487f6dd8  setup_space_quest2_2.1.0.16.exe
  c2446b38457104c209a32af03501dc7a719b4078  setup_space_quest_2_-_vohauls_revenge_2.0f_(20240).exe
  7d9190e889b3bad89d5f69c1baae387566822d31  SQManual.pdf?steam
  935c17418e94af68dfe37a8be39a0b538e03ed3d  Manual.pdf?steam
...
