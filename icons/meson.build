# Copyright 2022 Simon McVittie
# SPDX-License-Identifier: FSFAP

install_data(
  'quake3-tango.png',
  'quake3-teamarena-tango.png',
  'soltys.png',
  install_dir : pkgdatadir,
)

quake_recolours = [
  {
    'game' : 'quake',
    'expansion' : 'dissolution',
    'sed' : [
      's/#c17d11/#999984/',
      's/#d5b582/#dede95/',
      's/#5f3b01/#403f31/',
      's/#e9b96e/#dede95/',
    ],
  },
  {
    'game' : 'quake',
    'expansion' : 'armagon',
    'sed' : [
      's/#c17d11/#565248/',
      's/#d5b582/#aba390/',
      's/#5f3b01/#000000/',
      's/#e9b96e/#aba390/',
    ],
  },
  {
    'game' : 'quake2',
    'expansion' : 'reckoning',
    'sed' : [
      's/#3a5a1e/#999984/',
      's/#73ae3a/#eeeeec/',
      's/#8ae234/#eeeeec/',
      's/#132601/#233436/',
    ],
  },
  {
    'game' : 'quake2',
    'expansion' : 'groundzero',
    'sed' : [
      's/#3a5a1e/#ce5c00/',
      's/#73ae3a/#fce94f/',
      's/#8ae234/#fce94f/',
      's/#132601/#cc0000/',
    ],
  },
]

quake_icons = []

foreach game : ['quake', 'quake2', 'quake4']
  quake_icons += {
    'game' : game,
    'name' : game,
    'svg' : files('quake1+2.svg'),
  }
endforeach

foreach recolour : quake_recolours
  command = ['sed']

  foreach expr : recolour['sed']
    command += ['-e', expr]
  endforeach

  command += '@INPUT@'

  quake_icons += {
    'game' : recolour['game'],
    'name' : recolour['game'] + '-' + recolour['expansion'],
    'svg' : custom_target(
      'recolour-' + recolour['expansion'] + '.svg',
      build_by_default : true,
      input : files('quake1+2.svg'),
      output : 'recolour-' + recolour['expansion'] + '.svg',
      command : command,
      capture : true,
    ),
  }
endforeach

foreach icon : quake_icons
  custom_target(
    icon['name'] + '.svg',
    build_by_default : true,
    input : icon['svg'],
    output : icon['name'] + '.svg',
    command : [
      python,
      meson.project_source_root() / 'tools' / 'export-svg-layer.py',
      '@INPUT@',
      'layer-' + icon['game'] + '-256',
      '@OUTPUT@',
    ],
    install : true,
    install_dir : get_option('datadir') / 'icons' / 'hicolor' / 'scalable' / 'apps',
  )
endforeach

subdir('256')
subdir('48')
subdir('32')
subdir('22')
subdir('16')
# must be after 22
subdir('24')

foreach svg : [
  'comi',
  'grimfandango',
  'inherit',
  'kingsquest',
  'kyrandia2',
  'maniacmansion',
  'spacequest',
  'tentacle',
  'wolf-common',
  'zork-inquisitor',
  'zork-nemesis',
]
  custom_target(
    svg + '.png',
    build_by_default : true,
    input : svg + '.svg',
    output : svg + '.png',
    command : [
      'inkscape',
      '-w', '96',
      '-h', '96',
      '--export-filename=@OUTPUT@',
      '@INPUT@',
    ],
    install : true,
    install_dir : pkgdatadir,
  )
  simplified_svg = custom_target(
    svg + '.svg',
    build_by_default : true,
    input : svg + '.svg',
    output : svg + '.svg',
    command : [
      'inkscape',
      '--export-plain-svg',
      '--export-filename=@OUTPUT@',
      '@INPUT@',
    ],
  )
  custom_target(
    svg + '.svgz',
    build_by_default : true,
    input : simplified_svg,
    output : svg + '.svgz',
    command : ['gzip', '-nc', '@INPUT@'],
    capture : true,
    install : true,
    install_dir : pkgdatadir,
  )
endforeach

foreach xpm : [
  'doom-common',
  'draci',
  'sfinx',
]
  custom_target(
    xpm + '.png',
    build_by_default : true,
    input : xpm + '.xpm',
    output : xpm + '.png',
    command : ['convert', '@INPUT@', '@OUTPUT@'],
    install : true,
    install_dir : pkgdatadir,
  )
endforeach

custom_target(
  'doom2-masterlevels.png',
  build_by_default : true,
  input : 'doom-common.xpm',
  output : 'doom2-masterlevels.png',
  command : ['convert', '@INPUT@', '@OUTPUT@'],
  install : true,
  install_dir : get_option('datadir') / 'pixmaps',
)
