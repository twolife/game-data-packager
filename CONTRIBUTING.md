# Contributing to game-data-packager

Please send merge requests to
<https://salsa.debian.org/games-team/game-data-packager>,
or bug reports to the Debian bug tracking system.

## Compatibility guidelines

The oldest dependency versions we aim to support are whatever is in
Debian stable or Ubuntu LTS, whichever is older. As of mid 2023 this means:

* Debian 12: Python 3.11
* Ubuntu 22.04: Python 3.10

## Coding style

Please follow [PEP8](https://www.python.org/dev/peps/pep-0008/) for new code.

## Python features intentionally not used yet

After Ubuntu 24.04 is released, we can start using Python 3.11 features:

* `Self` type: <https://realpython.com/python-type-self/>
* `TypedDict`: make items as optional <https://peps.python.org/pep-0655/>

After the baseline is Python >=3.12 is the baseline we can annotate **kwargs
with `Unpack` + `TypedDict`: <https://peps.python.org/pep-0692/>

## Checking typing annotations

    sudo apt install python3-typeshed
    sudo pip3 install pygobject-stubs --config-settings=config=Gtk4,Gdk4 --break-system-packages
