#!/bin/sh
# Copyright 2018-2023 Collabora Ltd.
# Copyright 2023 Simon McVittie
# SPDX-License-Identifier: FSFAP

set -eux
export DEBIAN_FRONTEND=noninteractive
apt-get -y update
apt-get -y build-dep .
apt-get -y install \
    mypy \
    pycodestyle \
    pyflakes3 \
    ${NULL+}

meson setup -Dwerror=true debian/tmp

if ! meson test -C debian/tmp --suite=lint -v; then
    exit 77
fi

# tests/mypy.sh is not yet connected up to the Meson build system, because
# we haven't yet reached a point where it can be expected to run without
# warnings. Run it for its text output, but don't fail the job if it reports
# warnings or errors yet.
./tests/mypy.sh
