#!/usr/bin/python3
# encoding=utf-8
#
# Copyright © 2024 Sébastien Noel <sebastien@twolife.be>
# SPDX-License-Identifier: GPL-2.0-or-later

from __future__ import annotations

import logging
import os
from typing import (TYPE_CHECKING)

from ..build import (PackagingTask)
from ..game import (GameData)
from ..util import (mkdir_p)

if TYPE_CHECKING:
    from typing import (Unpack)
    from ..packaging import (PerPackageState, PackagingTaskArgs)

logger = logging.getLogger(__name__)


class SC3UGameData(GameData):
    def construct_task(self, **kwargs: Unpack[PackagingTaskArgs]) -> SC3UShim:
        return SC3UShim(self, **kwargs)


class SC3UShim(PackagingTask):
    def fill_extra_files(
        self,
        per_package_state: PerPackageState
    ) -> None:
        super().fill_extra_files(per_package_state)
        package = per_package_state.package
        destdir = per_package_state.destdir

        if package.name == 'sc3u-data':
            # The game need these 2 empty directories
            gamedir = os.path.join(destdir, 'usr', 'share',
                                   'games', self.game.shortname)
            mkdir_p(os.path.join(gamedir, 'res/ba/ui/shared/dlg'))
            mkdir_p(os.path.join(gamedir, 'res/sound/radio/stations'))
            return


GAME_DATA_SUBCLASS = SC3UGameData
