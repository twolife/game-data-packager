# Copyright © 2016 Alexandre Detiste
# SPDX-License-Identifier: GPL-2.0-or-later

1) sample output of openmw-iniimporter without "Data Files"

cfg file does not exist
load ini file: "/tmp/gdptmp.k5ig_6ps/morrowind-fr-data.deb.d/usr/share/games/morrowind-fr/Morrowind.ini"
Warning: ignored empty value for key 'General:Beta Comment File'.
Warning: ignored empty value for key 'General:Editor Starting Cell'.
load cfg file: "/tmp/gdptmp.k5ig_6ps/morrowind-fr-data.deb.d/usr/share/games/morrowind-fr/openmw.cfg"
content file: "/tmp/gdptmp.k5ig_6ps/morrowind-fr-data.deb.d/usr/share/games/morrowind-fr/Data Files/Morrowind.esm" not found
write to: /tmp/gdptmp.k5ig_6ps/morrowind-fr-data.deb.d/usr/share/games/morrowind-fr/openmw.cfg

2) I can't 100% reproduce the openmw.cfg created by openmw

$ diff -uw .config/openmw/openmw.cfg /usr/share/games/morrowind-fr/openmw.cfg
--- .config/openmw/openmw.cfg
+++ /usr/share/games/morrowind-fr/openmw.cfg
@@ -1,5 +1,4 @@
-no-sound=0
-fallback-archive=Morrowind.bsa
+content=Morrowind.esm
 fallback=LightAttenuation_UseConstant,0
 fallback=LightAttenuation_ConstantValue,0.0
 fallback=LightAttenuation_UseLinear,1
@@ -494,6 +493,15 @@
 fallback=Moons_Masser_Fade_In_Finish,15
 fallback=Moons_Masser_Fade_Out_Start,7
 fallback=Moons_Masser_Fade_Out_Finish,10
-encoding=win1252
-data=/usr/share/games/morrowind-fr
-content=Morrowind.esm
+fallback=Blood_Model_0,BloodSplat.nif
+fallback=Blood_Model_1,BloodSplat2.nif
+fallback=Blood_Model_2,BloodSplat3.nif
+fallback=Blood_Texture_0,Tx_Blood.tga
+fallback=Blood_Texture_1,Tx_Blood_White.tga
+fallback=Blood_Texture_2,Tx_Blood_Gold.tga
+fallback=Blood_Texture_Name_0,Default (Red)
+fallback=Blood_Texture_Name_1,Skeleton (White)
+fallback=Blood_Texture_Name_2,Metal Sparks (Gold)
+fallback-archive=Morrowind.bsa
+no-sound=0
+data=/usr/share/games/morrowind-fr

