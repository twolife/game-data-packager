# SPDX-FileCopyrightText: 2022 Simon McVittie
# SPDX-License-Identifier: FSFAP

project(
  'game-data-packager',
  version : '83',
  meson_version : '>= 0.61.0',
  default_options : [
    'warning_level=3',
  ],
)

python = find_program('python3')

gamedatadir = get_option('gamedatadir')

if gamedatadir == ''
  gamedatadir = get_option('datadir')
endif

distro = get_option('distro')
format = get_option('format')
pkgdatadir = gamedatadir / meson.project_name()
bindir = get_option('bindir')
libdir = get_option('libdir')
runtimedir = gamedatadir / meson.project_name() + '-runtime'
program_prefix = get_option('program_prefix')
non_multiarch_libdir = get_option('non_multiarch_libdir')

install_symlinks = []

configure_file(
  input : 'tools/gdp.in',
  output : 'game-data-packager',
  configuration : {
    'pkgdatadir' : get_option('prefix') / pkgdatadir,
  },
  install : true,
  install_dir : bindir,
  install_mode : 'rwxr-xr-x',
)

configure_file(
  input : 'tools/run-uninstalled.in',
  output : 'run-gdp-uninstalled',
  configuration : {
    'srcdir' : meson.project_source_root(),
    'builddir' : meson.project_build_root(),
    'command' : 'python3 -m game_data_packager.command_line',
  },
)

configure_file(
  input : 'tools/run-uninstalled.in',
  output : 'run-tool-uninstalled',
  configuration : {
    'srcdir' : meson.project_source_root(),
    'builddir' : meson.project_build_root(),
    'command' : '',
  },
)

version_py = custom_target(
  'version.py',
  build_by_default : true,
  capture : true,
  command : [
    python,
    files('game_data_packager/version.py'),
    '--distro=' + get_option('distro'),
    '--format=' + get_option('format'),
    '--version=' + meson.project_version(),
  ],
  output : 'version.py',
  install : true,
  install_dir : pkgdatadir / 'game_data_packager',
)

configure_file(
  copy : true,
  input : 'data' / 'copyright',
  output : 'copyright',
  install : true,
  install_dir : pkgdatadir,
)

foreach package : ['descent2-demo-data']
  configure_file(
    copy : true,
    input : 'data' / package + '.copyright',
    output : package + '.copyright',
    install : true,
    install_dir : pkgdatadir,
  )
endforeach

subdir('data')
subdir('doc')
subdir('game_data_packager')
subdir('icons')
subdir('runtime')

make_vfs_zip = [
  python,
  files('tools/make-vfs-zip.py'),
  '@OUTPUT@',
] + vfs_files

vfs_zip = custom_target(
  'vfs.zip',
  build_by_default : true,
  command : make_vfs_zip,
  input : vfs_files,
  output : 'vfs.zip',
  install : true,
  install_dir : pkgdatadir,
)

make_scummvm_runtime_data = [
  'env',
  'PYTHONPATH=' + meson.project_source_root(),
  python,
  files('tools/make-runtime-scummvm-data.py'),
  '@INPUT@',
  '@OUTPUT@',
]

custom_target(
  'launch_scummvm.zip',
  build_by_default : true,
  command : make_scummvm_runtime_data,
  input : vfs_zip,
  output : ['launch_scummvm.zip', 'scummvm'],
  install : true,
  install_dir : runtimedir,
)

custom_target(
  'bash_completion',
  build_by_default : true,
  capture : true,
  command : [python, files('tools/bash_completion.py')],
  depend_files : supported_games_yaml,
  output : 'bash_completion',
  install : true,
  install_dir : pkgdatadir,
)

custom_target(
  'changelog.gz',
  build_by_default : true,
  capture : true,
  command : ['gzip', '-nc9', '@INPUT@'],
  input : meson.project_source_root() / 'debian' / 'changelog',
  output : 'changelog.gz',
  install : true,
  install_dir : pkgdatadir,
)

subdir('etc')

foreach game : [
  'gdp.doom2_masterlevels',
  'etqw',
  'quake2',
  'quake2-groundzero',
  'quake2-reckoning',
  'quake2-zaero',
  'quake3',
  'quake3-team-arena',
  'quake3-threewave',
  'quake4',
  'quake-aopfm',
  'quake-armagon',
  'quake',
  'quake-dissolution',
  'quake-dopa',
]
  if game.startswith('gdp.')
    appid = 'net.debian.game_data_packager.' + game.substring(4)
  else
    appid = game
  endif

  custom_target(
    appid + '.desktop',
    input : 'runtime' / game + '.desktop.in',
    output : appid + '.desktop',
    command : [
      'env',
      'PYTHONPATH=' + meson.project_source_root(),
      files(meson.project_source_root() / 'tools' / 'expand_vars.py'),
      '@INPUT@',
      '@OUTPUT@',
    ],
    install : true,
    install_dir : get_option('datadir') / 'applications',
  )
endforeach

foreach game : [
  'civctp',
  'dhewm3-cdoom',
  'dhewm3-d3le',
  'heavy-gear2',
  'heretic2',
  'kohan',
  'mohaa',
  'mohaa-breakthrough',
  'mohaa-spearhead',
  'prey',
  'railroad-tycoon2',
  'rune',
  'sc3bat',
  'sc3u',
  'smac',
  'smacpack',
  'smacx',
  'soldier-of-fortune',
  'unreal',
  'unreal-gold',
  'ut2004',
  'ut99',
]
  custom_target(
    game + '.desktop',
    input : 'runtime' / game + '.desktop.in',
    output : game + '.desktop',
    command : [
      'env',
      'PYTHONPATH=' + meson.project_source_root(),
      files(meson.project_source_root() / 'tools' / 'expand_vars.py'),
      '@INPUT@',
      '@OUTPUT@',
    ],
    install : true,
    install_dir : runtimedir,
  )
endforeach

foreach exe : [
  'aliens-versus-predator',
  'civctp',
  'etqw',
  'etqw-dedicated',
  'heavy-gear2',
  'heretic2',
  'kohan',
  'mohaa',
  'mohaa-server',
  'openjk',
  'openjk_sp',
  'openjkded',
  'openjo_sp',
  'prey',
  'quake',
  'quake-server',
  'quake2',
  'quake2-server',
  'quake3',
  'quake3-server',
  'quake4',
  'quake4-dedicated',
  'railroad-tycoon2',
  'rbdoom3bfg',
  'rune',
  'sc3bat',
  'sc3u',
  'serious-sam-tfe',
  'serious-sam-tse',
  'smac',
  'smacx',
  'soldier-of-fortune',
  'unreal',
  'unreal-gold',
  'ut2004',
  'ut99',
  'wolfded',
  'wolfmp',
  'wolfsp',
]
  yaml = custom_target(
    'launch-' + exe + '.yaml',
    input : 'runtime' / 'launch-' + exe + '.yaml.in',
    output : 'launch-' + exe + '.yaml',
    command : [
      'env',
      'PYTHONPATH=' + meson.project_source_root(),
      files(meson.project_source_root() / 'tools' / 'expand_vars.py'),
      '@INPUT@',
      '@OUTPUT@',
    ],
  )
  custom_target(
    'launch-' + exe + '.json',
    input : yaml,
    output : 'launch-' + exe + '.json',
    command : [
      'env',
      'PYTHONPATH=' + meson.project_source_root(),
      files(meson.project_source_root() / 'tools' / 'yaml2json.py'),
      '@INPUT@',
      '@OUTPUT@',
    ],
    install : true,
    install_dir : runtimedir,
  )
endforeach

subdir('tests')

foreach symlink : install_symlinks
  if not symlink['pointing_to'].startswith('/')
    install_symlink(
      symlink['link_name'],
      install_dir : symlink['install_dir'],
      pointing_to : symlink['pointing_to'],
    )
  else
    meson.add_install_script(
      'tools/meson-compat-install-symlink.sh',
      symlink['link_name'],
      symlink['install_dir'],
      symlink['pointing_to'],
    )
  endif
endforeach

cpu_family = host_machine.cpu_family()
if cpu_family == 'x86'
    add_languages('c', native: false)
    shared_library(
        'lokishim', 'lokishim/lokishim.c', name_prefix: '', install: true,
        install_dir : get_option('libdir') / meson.project_name(),
    )
    shared_library(
        'sc3u-nptl', 'lokishim/sc3u-nptl.c', name_prefix: '', install: true,
        install_dir : get_option('libdir') / meson.project_name(),
        link_args: ['-nostartfiles']
    )
endif
