#!/usr/bin/python3
# encoding=utf-8
#
# Copyright © 2015 Alexandre Detiste <alexandre@detiste.be>
# SPDX-License-Identifier: GPL-2.0-or-later

import os
import subprocess
import time

import distro_info
from debian.changelog import Changelog

# BASE = '/home/tchet/git'
# GDP = os.path.join(BASE, 'game-data-packager')

GDP = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
BASE = os.path.dirname(GDP)

# Support for DebHelper and DH-Cruft
DH_VERSIONS = {
    'xenial': (9, False),
    'jammy': (13, False),
    'kinetic': (13, False),
    'lunar': (13, True),
}

subprocess.check_call(['rm', '-rvf', 'ref.zip'],
                      cwd=GDP)
subprocess.check_call(['rm', '-rvf', 'build'],
                      cwd=GDP)
subprocess.check_call(['git', 'checkout', 'debian/changelog'],
                      cwd=GDP)
subprocess.check_call(['git', 'checkout', 'debian/control'],
                      cwd=GDP)

with open('debian/changelog', encoding='utf-8') as log:
    cl = Changelog(log, strict=False)

assert cl.distributions in ('unstable', 'UNRELEASED'), cl.distributions

if cl.distributions == 'unstable':
    build_type = 'Backport to PPA'
    build_number = 'release+'
else:
    build_type = 'Git snapshot'
    build_number = time.strftime('git%Y%m%d+')

lts = str(distro_info.UbuntuDistroInfo().lts())
current = str(distro_info.UbuntuDistroInfo().stable())

releases = sorted(set([lts, current]))
print('RELEASES:', releases)

with open('debian/control', 'r') as compat:
    for line in compat:
        if 'debhelper-compat' in line:
            current_debhelper = int(line.split('(')[1].strip(' =),\n'))
            break

for release in sorted(releases):
    supported_dh = DH_VERSIONS.get(release, [current_debhelper, False])
    supported_debhelper = supported_dh[0]
    supported_dh_cruft = supported_dh[1]
    backported = set()

    if supported_debhelper < current_debhelper:
        backported.add('control')
        build_dep = 'debhelper-compat ( = %d)' % supported_debhelper
        subprocess.check_call(['sed', '-i',
                               r's/\ *debhelper-compat.*/ ' + build_dep + ',/',
                               'debian/control'],
                              cwd=GDP)

    if not supported_dh_cruft:
        backported.add('control')
        subprocess.check_call(['sed', '-i', '/dh-sequence-cruft/d',
                              'debian/control'],
                              cwd=GDP)

    snapshot = str(cl.version) + '~' + build_number + release
    subprocess.check_call(['dch', '-b',
                           '-D', release,
                           '-v', snapshot,
                           build_type],
                          cwd=GDP)

    subprocess.check_call(['debuild', '-S', '-i'], cwd=GDP)
    subprocess.check_call(['dput', 'my-ppa',
                           'game-data-packager_%s_source.changes' % snapshot],
                          cwd=BASE)

    subprocess.check_call(['git', 'checkout', 'debian/changelog'],
                          cwd=GDP)

    for item in backported:
        subprocess.check_call(['git', 'checkout', 'debian/' + item],
                              cwd=GDP)
    for file in ('.tar.xz',
                 '.dsc',
                 '_source.build',
                 '_source.changes',
                 '_source.my-ppa.upload'):
        subprocess.check_call(['rm', '-v',
                               'game-data-packager_%s%s' % (snapshot, file)],
                              cwd=BASE)
