# Quake 4 client AppArmor profile
# Copyright © 2016-2020 Simon McVittie
# SPDX-License-Identifier: FSFAP

#include <tunables/global>

profile quake4 /usr/lib/quake4/quake4{,smp}.x86 flags=(complain) {
  #include <abstractions/X>
  #include <abstractions/audio>
  #include <abstractions/base>
  #include <abstractions/dri-common>
  #include <abstractions/dri-enumerate>
  #include <abstractions/mesa>
  #include <abstractions/nameservice>
  #include <abstractions/nvidia>
  #include <abstractions/private-files-strict>
  #include <abstractions/ubuntu-helpers>

  network inet dgram,
  network inet stream,
  network inet6 dgram,
  network inet6 stream,

  /usr/lib/quake4/quake4.x86 mr,
  /usr/lib/quake4/quake4smp.x86 mr,
  /usr/lib/quake4/libSDL-1.2.id.so.0 mr,
  /usr/share/games/quake4/** r,
  owner @{HOME}/.quake4/** rwk,
  owner @{HOME}/.quake4/*/gamex86.so rwkm,

  # used by PulseAudio
  /etc/machine-id r,
  /var/lib/dbus/machine-id r,

  # the audio and X abstractions don't allow mmapping these
  /dev/dri/* m,
  owner /{run,dev}/shm/pulse-shm* m,

  # udev device enumeration, input devices, video
  /etc/udev/udev.conf r,
  /run/udev/data/** r,
  @{sys}/bus/ r,
  @{sys}/class/ r,
  @{sys}/class/drm/ r,
  @{sys}/class/input/ r,
  @{sys}/class/sound/ r,
  @{sys}/devices/**/drm/** r,
  @{sys}/devices/**/input/** r,
  @{sys}/devices/**/sound/**/input*/** r,
  @{sys}/devices/**/sound/**/uevent r,
  @{sys}/devices/pci*/**/config r,
  @{sys}/devices/pci*/**/revision r,

  /usr/bin/xdg-open Cxr -> xdgopen,
  /usr/share/games/game-data-packager-runtime/gdp-openurl Cxr -> xdgopen,

  profile xdgopen flags=(complain) {
    #include <abstractions/base>
    #include <abstractions/dbus-session-strict>
    #include <abstractions/ubuntu-browsers>

    /usr/bin/xdg-open rm,
    /{usr/,}bin/dash rmix,

    /usr/share/games/game-data-packager-runtime/gdp-openurl rm,
    /usr/bin/python3 rmix,
    dbus (send) bus=session peer=(name=org.freedesktop.portal.Desktop),
  }
}
